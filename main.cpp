#include "petrinetvhdl.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PetriNetVHDL w;
    w.show();

    return a.exec();
}
