#include "petrinetvhdl.h"
#include "ui_petrinetvhdl.h"

PetriNetVHDL::PetriNetVHDL(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PetriNetVHDL)
{
    ui->setupUi(this);
    fileVHDL.close();
    vInicializaMatrizInterface();
    vInicializaMatriz();
}

PetriNetVHDL::~PetriNetVHDL()
{
    delete ui;
}

void PetriNetVHDL::vInicializaMatrizInterface()
{
    int nLugares, nTransicoes;
    nLugares = nTransicoes = 4;

    ui->sb_Lugares->setValue(nLugares);
    ui->sb_Transicoes->setValue(nLugares);

    strlLabelsLugares.clear(); strlLabelsTransicoes.clear();

    for (int n = 0; n < 4; ++n)
    {
        strlLabelsLugares << "L" + QString::number(n);
        strlLabelsTransicoes << "t" + QString::number(n);
    }

    ui->tbw_MatrizIncidencia->setColumnCount(nLugares);
    ui->tbw_MatrizIncidencia->setRowCount(nTransicoes);
    ui->tbw_MatrizIncidencia->verticalHeader()->setVisible(true);
    ui->tbw_MatrizIncidencia->horizontalHeader()->setVisible(true);

    ui->tbw_MatrizIncidencia->setHorizontalHeaderLabels(strlLabelsLugares);
    ui->tbw_MatrizIncidencia->setVerticalHeaderLabels(strlLabelsTransicoes);

    for(int i=0; i < nLugares; ++i)
    {
        for(int j=0; j < nTransicoes; ++j)
        {
            QTableWidgetItem *tbwiAux = new QTableWidgetItem(0);
//            tbwiAux->setBackground(Qt::green);
            ui->tbw_MatrizIncidencia->setItem(j,i, tbwiAux);
            ui->tbw_MatrizIncidencia->item(j, i)->setData(0, 0);
        }
    }

}

void PetriNetVHDL::vInicializaMatriz()
{
    int nLugares    = ui->sb_Lugares->value(),
        nTransicoes = ui->sb_Transicoes->value();

    matrizIncidencia = std::vector<std::vector<indiceMatriz> >(nLugares, std::vector<indiceMatriz>(nTransicoes));
    tipoLugar = QVector<TipoLugar>(nLugares);
    tamanhoMatriz.first  = nLugares;
    tamanhoMatriz.second = nTransicoes;
}

void PetriNetVHDL::vAtualizaMatrizInterface()
{
    int nLugares = tamanhoMatriz.first,
        nTransicoes = tamanhoMatriz.second,
        nLugaresAtual    = ui->tbw_MatrizIncidencia->columnCount(),
        nTransicoesAtual = ui->tbw_MatrizIncidencia->rowCount();

    ui->tbw_MatrizIncidencia->setColumnCount(nLugares);
    ui->tbw_MatrizIncidencia->setRowCount(nTransicoes);

    ui->tbw_MatrizIncidencia->setHorizontalHeaderLabels(strlLabelsLugares);
    ui->tbw_MatrizIncidencia->setVerticalHeaderLabels(strlLabelsTransicoes);

    for(int i=0; i < nLugares; ++i)
    {
        for(int j=0; j < nTransicoes; ++j)
        {
            if(nLugaresAtual < nLugares && i >= nLugaresAtual)
            {
                QTableWidgetItem *tbwiAux = new QTableWidgetItem(0);
                ui->tbw_MatrizIncidencia->setItem(j,i, tbwiAux);
            }
            else if(nTransicoesAtual < nTransicoes && j >= nTransicoesAtual)
            {
                QTableWidgetItem *tbwiAux = new QTableWidgetItem(0);
                ui->tbw_MatrizIncidencia->setItem(j,i, tbwiAux);
            }

            ui->tbw_MatrizIncidencia->item(j, i)->setData(0, matrizIncidencia.at(i).at(j).valor);
        }
    }
}

void PetriNetVHDL::vGerarVHDL()
{
    QString vhdl = "";
    // Cria o IF de cada lugar

    for (int lugar = 0; lugar < tamanhoMatriz.first; ++lugar)
    {
        vhdl += "-- Lugar " + strlLabelsLugares.at(lugar) + "\n";
        // Ganha marcas
        vGanhaMarcas(vhdl,lugar);
        // Perde marcas
        vPerdeMarcas(vhdl,lugar);
        // Mantem
        vMantemMarcas(vhdl,lugar);
    }

    if(fileVHDL.isOpen())
        fileVHDL.close();

    QString fileName = QLatin1String("");
    fileName = QFileDialog::getSaveFileName(this, "Salvar VHDL", "RedePetri.txt", "txt files (.txt)", nullptr, QFileDialog::DontUseNativeDialog);
    if(fileName != "")
    {
        fileVHDL.setFileName(fileName);
        fileVHDL.open(QIODevice::ReadOnly | QIODevice::Text | QIODevice::ReadWrite);

        QTextStream stream(&fileVHDL);
        stream << vhdl << endl;
    }

}

void PetriNetVHDL::vGanhaMarcas(QString &vhdl, int lugar)
{
    bool colocarElsif = false;
    QPair<QString, indiceMatriz> lugarInicial;
    lugarInicial.first = "";

    //Procura as transições para ganhar marcas
    for (int t = 0; t < tamanhoMatriz.second; ++t)
    {
        QString condicaoTransicao = "";

        if(matrizIncidencia.at(lugar).at(t).valor > 0)// Se o lugar ganha marcas com a transição t
        {
            //Procura os lugares para a transição disparar
            vMontaCondicoes(vhdl, lugar, colocarElsif, lugarInicial, t);

            // Só adiciona a transição caso ela não esteja sempre ativada
            if(strlLabelsTransicoes.at(t) != "1")
                condicaoTransicao = " AND " + strlLabelsTransicoes.at(t) + " = '1'";

            QString atribuicaoSaida = strlLabelsLugares.at(lugar) + "+" + QString::number(matrizIncidencia.at(lugar).at(t).valor);

            if(tipoLugar.at(lugar) == Binario)
                atribuicaoSaida = "'1'";

            vhdl += condicaoTransicao + " then " +
                    strlLabelsLugares.at(lugar) + " <= " + atribuicaoSaida + "; -- Ganha Marcas\n" ;

        }
    }
    colocarElsif = false;
}

void PetriNetVHDL::vPerdeMarcas(QString &vhdl, int lugar)
{
    QVector<QPair<QString, indiceMatriz>> lugaresCondicao;
    QPair<QString, indiceMatriz> pairAux;
    lugaresCondicao.clear();

    //Procura as transições para ganhar marcas
    for (int t = 0; t < tamanhoMatriz.second; ++t)
    {
        QString condicaoTransicao = "";

        // Um lugar perde marcas para uma transição t se o peso for negativo e não for um arco de teste/ for um arco comum
        if(matrizIncidencia.at(lugar).at(t).valor < 0 && matrizIncidencia.at(lugar).at(t).tipo == Comum)
        {
            //Adiciona o lugar na lista de condições
            pairAux.first  = strlLabelsLugares.at(lugar);
            pairAux.second.valor = matrizIncidencia.at(lugar).at(t).valor;
            pairAux.second.tipo  = matrizIncidencia.at(lugar).at(t).tipo;
            lugaresCondicao.append(pairAux);

            //Procura os lugares para a transição disparar
            bool colocarElsif = true;
            vMontaCondicoes(vhdl, lugar, colocarElsif, pairAux, t);

            // Só adiciona a transição caso ela não esteja sempre ativada
            if(strlLabelsTransicoes.at(t) != "1")
                condicaoTransicao = " AND " + strlLabelsTransicoes.at(t) + " = '1'";

            QString atribuicaoSaida = strlLabelsLugares.at(lugar) + "-" + QString::number(qAbs(matrizIncidencia.at(lugar).at(t).valor));

            if(tipoLugar.at(lugar) == Binario)
                atribuicaoSaida = "'0'";

            vhdl += condicaoTransicao + " then " +
                    strlLabelsLugares.at(lugar) + " <= " + atribuicaoSaida + "; -- Perde Marcas\n" ;

        }
    }
}

void PetriNetVHDL::vMantemMarcas(QString &vhdl, int lugar)
{
    vhdl += "else " + strlLabelsLugares.at(lugar) + " <= " + strlLabelsLugares.at(lugar) + "; -- Mantem Marcas\n"+"end if;\n\n";
}

void PetriNetVHDL::vMontaCondicoes(QString &vhdl, int lugar, bool &colocarElsif, QPair<QString, indiceMatriz> lugarInicial, int transicao)
{
    QVector<QPair<QString, indiceMatriz>> lugaresCondicao;
    QPair<QString, indiceMatriz> pairAux;
    lugaresCondicao.clear();

    if(lugarInicial.first != "")
        lugaresCondicao.append(lugarInicial);

    for (int cond = 0; cond < tamanhoMatriz.first; ++cond)
    {
        if(cond != lugar)
        {
            if(matrizIncidencia.at(cond).at(transicao).valor < 0)
            {
                pairAux.first  = strlLabelsLugares.at(cond);
                pairAux.second.valor = matrizIncidencia.at(cond).at(transicao).valor;
                pairAux.second.tipo  = matrizIncidencia.at(cond).at(transicao).tipo;
                lugaresCondicao.append(pairAux);
            }
        }
    }

    if(colocarElsif == false)
    {
        vhdl += "if ";
        colocarElsif = true;
    }
    else
    {
        vhdl += "elsif ";
    }
    for (int condicoes = 0; condicoes < lugaresCondicao.size(); ++condicoes)
    {
        if(condicoes > 0)
            vhdl += " AND ";

        QString comparador = " > ";
        QString valor = QString::number(qAbs(lugaresCondicao.at(condicoes).second.valor)-1);
        int indiceLugar = strlLabelsLugares.indexOf(lugaresCondicao.at(condicoes).first);

        // Se for um arco inibidor, inverte-se a condição e não subtraimos 1 se for inteiro
        if(lugaresCondicao.at(condicoes).second.tipo == Inibidor)
        {
            if(tipoLugar.at(indiceLugar) == Inteiro)
            {
                comparador = " < ";
                valor = QString::number(qAbs(lugaresCondicao.at(condicoes).second.valor));
            }
            else
            {
                comparador = " = ";
                valor = "'" + QString::number(qAbs(lugaresCondicao.at(condicoes).second.valor)-1) + "'";
            }
        }
        else if(tipoLugar.at(indiceLugar) == Binario)
        {
            comparador = " = ";
            valor = "'" + QString::number(qAbs(lugaresCondicao.at(condicoes).second.valor)) + "'";
        }

        vhdl += lugaresCondicao.at(condicoes).first + comparador + valor;
    }
}

void PetriNetVHDL::on_sb_Lugares_editingFinished()
{
    int nLugarNovo = ui->sb_Lugares->value();

    if(nLugarNovo < tamanhoMatriz.first)
    {
        for (int n = tamanhoMatriz.first; n <= nLugarNovo; ++n)
        {
            matrizIncidencia.pop_back();
            strlLabelsLugares.pop_back();
        }
        tamanhoMatriz.first = nLugarNovo;
    }
    else if(nLugarNovo > tamanhoMatriz.first)
    {
        for (int n = tamanhoMatriz.first; n <= nLugarNovo; ++n)
        {
            matrizIncidencia.push_back(std::vector<indiceMatriz>(tamanhoMatriz.second));
            strlLabelsLugares.append("L"+QString::number(n));
        }
        tamanhoMatriz.first = nLugarNovo;
    }
    vAtualizaMatrizInterface();
}

void PetriNetVHDL::on_sb_Transicoes_editingFinished()
{
    int nTransicaoNovo = ui->sb_Transicoes->value();

    if(nTransicaoNovo < tamanhoMatriz.second)
    {
        for (int n = tamanhoMatriz.second; n <= nTransicaoNovo; ++n)
        {
            matrizIncidencia.end()->pop_back();
            strlLabelsTransicoes.pop_back();
        }
        tamanhoMatriz.second = nTransicaoNovo;
    }
    else if(nTransicaoNovo > tamanhoMatriz.second)
    {
        for (int L = 0; L < tamanhoMatriz.first; ++L)
        {
            matrizIncidencia.at(L).clear();
            if(L == 0)
                strlLabelsTransicoes.clear();

            for (int n = 0; n < nTransicaoNovo; ++n)
            {
                matrizIncidencia.at(L).push_back(indiceMatriz());

                if(L == 0)
                    strlLabelsTransicoes.append("t"+QString::number(n));
            }
        }

        tamanhoMatriz.second = nTransicaoNovo;
    }
    vAtualizaMatrizInterface();
}

void PetriNetVHDL::on_pb_GerarVHDL_clicked()
{
    QTableWidgetItem *tbwiAux = new QTableWidgetItem(false);


    for (int lugar = 0; lugar < tamanhoMatriz.first; ++lugar)
    {
        for (int tran = 0; tran < tamanhoMatriz.second; ++tran)
        {
            tbwiAux = ui->tbw_MatrizIncidencia->item(tran, lugar);
            matrizIncidencia.at(lugar).at(tran).valor = tbwiAux->data(0).toInt();
        }
    }

    vGerarVHDL();
}

void PetriNetVHDL::on_tbw_MatrizIncidencia_cellDoubleClicked(int row, int column)
{
    // Troca o tipo do arco
    // Comum -> Teste -> Inibidor -> Comum

    if(matrizIncidencia.at(column).at(row).tipo == Comum)
    {
        matrizIncidencia.at(column).at(row).tipo = Teste;
        ui->tbw_MatrizIncidencia->item(row, column)->setBackground(Qt::green);
    }
    else if(matrizIncidencia.at(column).at(row).tipo == Teste)
    {
        matrizIncidencia.at(column).at(row).tipo = Inibidor;
        ui->tbw_MatrizIncidencia->item(row, column)->setBackground(Qt::red);
    }
    else
    {
        matrizIncidencia.at(column).at(row).tipo = Comum;
        ui->tbw_MatrizIncidencia->item(row, column)->setBackground(Qt::transparent);
    }

}

void PetriNetVHDL::on_pb_EditarLugares_clicked()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("Insira o nome dos lugares separados por ';'"),
                                         tr("Lugares:"), QLineEdit::Normal, "L0;L1;...",&ok);
    if (ok && !text.isEmpty())
    {
        strlLabelsLugares.clear();
        strlLabelsLugares = text.split(";");
        ui->tbw_MatrizIncidencia->setHorizontalHeaderLabels(strlLabelsLugares);
    }
}

void PetriNetVHDL::on_pb_EditarTransicoes_clicked()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("Insira o nome das transições separados por ';'"),
                                         tr("Transições:"), QLineEdit::Normal, "t0;t1;..." ,&ok);
    if (ok && !text.isEmpty())
    {
        strlLabelsTransicoes.clear();
        strlLabelsTransicoes = text.split(";");
        ui->tbw_MatrizIncidencia->setVerticalHeaderLabels(strlLabelsTransicoes);
    }
}

void PetriNetVHDL::on_pb_TipoLugares_clicked()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("Insira o tipo dos lugares (Binario [B], Inteiro [I]) separados por ';'"),
                                         tr("Tipo:"), QLineEdit::Normal, "B;I;B;I;...",&ok);
    if (ok && !text.isEmpty())
    {
        QStringList tipos = text.split(";");
        tipoLugar.clear();

        foreach (auto tipo, tipos)
        {
            if(tipo == "B")
            {
                tipoLugar.append(Binario);
            }
            else if(tipo == "I")
            {
                tipoLugar.append(Inteiro);
            }
        }
    }
}
