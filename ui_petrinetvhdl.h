/********************************************************************************
** Form generated from reading UI file 'petrinetvhdl.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PETRINETVHDL_H
#define UI_PETRINETVHDL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PetriNetVHDL
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QSpinBox *sb_Lugares;
    QSpinBox *sb_Transicoes;
    QPushButton *pb_GerarVHDL;
    QPushButton *pb_EditarLugares;
    QPushButton *pb_TipoLugares;
    QPushButton *pb_EditarTransicoes;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QTableWidget *tbw_MatrizIncidencia;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *PetriNetVHDL)
    {
        if (PetriNetVHDL->objectName().isEmpty())
            PetriNetVHDL->setObjectName(QString::fromUtf8("PetriNetVHDL"));
        PetriNetVHDL->resize(737, 440);
        centralWidget = new QWidget(PetriNetVHDL);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        sb_Lugares = new QSpinBox(centralWidget);
        sb_Lugares->setObjectName(QString::fromUtf8("sb_Lugares"));
        sb_Lugares->setMaximumSize(QSize(16777215, 25));
        sb_Lugares->setMaximum(30);

        horizontalLayout->addWidget(sb_Lugares);

        sb_Transicoes = new QSpinBox(centralWidget);
        sb_Transicoes->setObjectName(QString::fromUtf8("sb_Transicoes"));
        sb_Transicoes->setMaximumSize(QSize(16777215, 25));
        sb_Transicoes->setMaximum(30);

        horizontalLayout->addWidget(sb_Transicoes);

        pb_GerarVHDL = new QPushButton(centralWidget);
        pb_GerarVHDL->setObjectName(QString::fromUtf8("pb_GerarVHDL"));
        pb_GerarVHDL->setMinimumSize(QSize(89, 25));

        horizontalLayout->addWidget(pb_GerarVHDL);

        pb_EditarLugares = new QPushButton(centralWidget);
        pb_EditarLugares->setObjectName(QString::fromUtf8("pb_EditarLugares"));
        pb_EditarLugares->setMinimumSize(QSize(106, 25));

        horizontalLayout->addWidget(pb_EditarLugares);

        pb_TipoLugares = new QPushButton(centralWidget);
        pb_TipoLugares->setObjectName(QString::fromUtf8("pb_TipoLugares"));

        horizontalLayout->addWidget(pb_TipoLugares);

        pb_EditarTransicoes = new QPushButton(centralWidget);
        pb_EditarTransicoes->setObjectName(QString::fromUtf8("pb_EditarTransicoes"));
        pb_EditarTransicoes->setMinimumSize(QSize(124, 25));

        horizontalLayout->addWidget(pb_EditarTransicoes);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_2->addWidget(label_3);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QPalette palette;
        QBrush brush(QColor(78, 154, 6, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        QBrush brush1(QColor(255, 255, 255, 127));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_4->setPalette(palette);

        horizontalLayout_2->addWidget(label_4);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QPalette palette1;
        QBrush brush2(QColor(239, 41, 41, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        label_5->setPalette(palette1);

        horizontalLayout_2->addWidget(label_5);


        verticalLayout->addLayout(horizontalLayout_2);

        tbw_MatrizIncidencia = new QTableWidget(centralWidget);
        tbw_MatrizIncidencia->setObjectName(QString::fromUtf8("tbw_MatrizIncidencia"));

        verticalLayout->addWidget(tbw_MatrizIncidencia);

        PetriNetVHDL->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(PetriNetVHDL);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 737, 22));
        PetriNetVHDL->setMenuBar(menuBar);
        mainToolBar = new QToolBar(PetriNetVHDL);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        PetriNetVHDL->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(PetriNetVHDL);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        PetriNetVHDL->setStatusBar(statusBar);

        retranslateUi(PetriNetVHDL);

        QMetaObject::connectSlotsByName(PetriNetVHDL);
    } // setupUi

    void retranslateUi(QMainWindow *PetriNetVHDL)
    {
        PetriNetVHDL->setWindowTitle(QCoreApplication::translate("PetriNetVHDL", "PetriNetVHDL", nullptr));
        sb_Lugares->setPrefix(QCoreApplication::translate("PetriNetVHDL", "N. Lugares: ", nullptr));
        sb_Transicoes->setPrefix(QCoreApplication::translate("PetriNetVHDL", "N. Transi\303\247\303\265es: ", nullptr));
        pb_GerarVHDL->setText(QCoreApplication::translate("PetriNetVHDL", "Gerar VHDL", nullptr));
        pb_EditarLugares->setText(QCoreApplication::translate("PetriNetVHDL", "Editar Lugares", nullptr));
        pb_TipoLugares->setText(QCoreApplication::translate("PetriNetVHDL", "Tipo Lugares", nullptr));
        pb_EditarTransicoes->setText(QCoreApplication::translate("PetriNetVHDL", "Editar Transi\303\247\303\265es", nullptr));
        label->setText(QCoreApplication::translate("PetriNetVHDL", "Matriz de incid\303\252ncia |", nullptr));
        label_2->setText(QCoreApplication::translate("PetriNetVHDL", "Legenda:", nullptr));
        label_3->setText(QCoreApplication::translate("PetriNetVHDL", "Branco = Comum", nullptr));
        label_4->setText(QCoreApplication::translate("PetriNetVHDL", "Verde = Teste", nullptr));
        label_5->setText(QCoreApplication::translate("PetriNetVHDL", "Vermelho = Inibidor", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PetriNetVHDL: public Ui_PetriNetVHDL {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PETRINETVHDL_H
