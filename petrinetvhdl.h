﻿#ifndef PETRINETVHDL_H
#define PETRINETVHDL_H

#include <QMainWindow>
#include <QFile>
#include <QDebug>
#include <QFileDialog>
#include <QInputDialog>

namespace Ui {
class PetriNetVHDL;
}

enum Arco
{
  Comum,
  Teste,
  Inibidor
};

enum TipoLugar
{
    Inteiro,
    Binario
};

typedef struct indiceMatriz
{
    int valor;
    Arco tipo;
    indiceMatriz()
    {
        valor = 0;
        tipo = Comum;
    }
}indiceMatriz;

class PetriNetVHDL : public QMainWindow
{
    Q_OBJECT

public:
    explicit PetriNetVHDL(QWidget *parent = nullptr);
    ~PetriNetVHDL();

private slots:
    void on_sb_Lugares_editingFinished();

    void on_sb_Transicoes_editingFinished();

    void on_pb_GerarVHDL_clicked();

    void on_tbw_MatrizIncidencia_cellDoubleClicked(int row, int column);

    void on_pb_EditarLugares_clicked();

    void on_pb_EditarTransicoes_clicked();

    void on_pb_TipoLugares_clicked();

private:
    Ui::PetriNetVHDL *ui;

    std::vector<std::vector<indiceMatriz> > matrizIncidencia;
    QVector<TipoLugar> tipoLugar; // Indica se o lugar eh binario
    QPair<int,int> tamanhoMatriz;
    QStringList strlLabelsLugares;
    QStringList strlLabelsTransicoes;

    QFile fileVHDL;

    void vInicializaMatrizInterface();
    void vInicializaMatriz();

    void vAtualizaMatrizInterface();

    void vGerarVHDL();

    void vGanhaMarcas(QString &vhdl, int lugar);
    void vPerdeMarcas(QString &vhdl, int lugar);
    void vMantemMarcas(QString &vhdl, int lugar);

    void vMontaCondicoes(QString &vhdl, int lugar, bool &colocarElsif, QPair<QString, indiceMatriz> lugarInicial, int transicao);
};

#endif // PETRINETVHDL_H
